
import {Fragment} from 'react';
import ProductCard from '../components/product-card'
import productsData from '../data/products-data';
import { Row, Col, Button, Form, FormControl, NavDropdown }from 'react-bootstrap';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';


export default function Products(){
	// console.log(coursesData);
	// console.log(coursesData[0]);
	const products = productsData.map(product =>{
		return(
			// key is to check the Ids in courseData
			<ProductCard key= {product.id}productProp ={product}/>
			)
	})

	return(
		<Col className="pt-5 mt-2 mr-0 ml-0">
		<Navbar expand="lg">
		    <Form inline className="d-flex">
		      <Button variant="outline-none">Search</Button>
		      <FormControl type="text" placeholder="Search" className="mr-sm-2" />
		      
		    </Form>
		    <Nav className="ml-auto col-4">
		      <NavDropdown title="Category" id="basic-nav-dropdown">
		        <NavDropdown.Item href="#action/3.1">Green Tea</NavDropdown.Item>
		        <NavDropdown.Item href="#action/3.2">Black tea</NavDropdown.Item>
		        <NavDropdown.Item href="#action/3.3">Fruitty Blends</NavDropdown.Item>
		        <NavDropdown.Item href="#action/3.4">Herbal Tea</NavDropdown.Item>
		      </NavDropdown>
		    </Nav>
		</Navbar>
		<Row className="dark-beige mt-5">
			<Fragment>
				{products}
			</Fragment>
		</Row>
		</Col>
		
		)
}

