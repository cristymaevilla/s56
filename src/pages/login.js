import {Fragment} from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import { Link } from 'react-router-dom';
import { Button, Form }from 'react-bootstrap';
import UserContext from '../user-context';
 

export default function loginimport(){

	
	return( 
		<div className="mt-5 pt-5  pb-4  pl-md-5 pr-md-5 vh-100 login">
			<div className="pl-5 pr-5 pt-4 ">
				<Form className="pt-5 align-items-center login-form pl-lg-5 pr-lg-5 col-lg-6">
				  <Form.Group className="mb-3 pl-0  login-form" controlId="formBasicEmail">
				    <Form.Label>Email Address</Form.Label>
				    <Form.Control type="email" placeholder="" className="form-control rounded-0"  />
				  </Form.Group>

				  <Form.Group className="mb-3 pl-0  login-form" controlId="formBasicPassword rounded-0">
				    <Form.Label>Password</Form.Label>
				    <Form.Control type="password" placeholder="" className="form-control rounded-0" />
				  </Form.Group>

				  <Button variant="dark" type="submit" className="rounded-0 float-right login-form">
				    Sign In
				  </Button>
				  <div className = "text-center d-flex"> Already have an account? 
				  	<Link to="/register"  className="text-register p-0 pl-2" style={{ textDecoration: "none", color: "#A34546" }}> Register
			
				  	</Link>
				  </div>

				</Form>
					
				</div>
	</div>




		)
}


