// import {useState} from 'react';

import PropTypes from 'prop-types'
import { Button, Card, Row, Col }from 'react-bootstrap';
import UserContext from '../user-context';
// import productImage from '../images/product.png'; 

export default function Dashboard(){

	
	return( 
			<Row className="mt-5 pt-3 vh-100 align-middle">
			<div  className="mt-5 pt-5 pt-lg-0 mt-xl-0 center col-12">
			
				<div className="d-flex ml-0 mr-0 col-12 justify-content-center align-items-center pb-2 pb-sm-0">
					<div className="light-beige d-sm-flex mb-2 col-12 col-lg-10 justify-content-center">
						<div className="admin-dashboard-number font-weight-bold pr-2  mb-0 pb-0"> 100 </div> 
						<div className="pt-md-5 m-0 pr-2 ">	   
				   			 <h2 className=" font-weight-bold mb-0"> USERS </h2 > 
				   			 <h5 className=""> Lorem ipsum dolor sit amet, consectetur adipisicing elit</h5>
						</div> 
						<div className="pt-0 pt-sm-5 col-8 col-lg-2 col-sm-3 order-sm-2 ">
				   			<div className="add-button text-center p-2 ml-2 float-left container-fluid">View All</div>
						</div>
					</div>
				</div>

				<div className="d-flex ml-0 mr-0 col-12 justify-content-center align-items-center pb-2 pb-sm-0">
					<div className="light-beige d-sm-flex mb-2 col-12 col-lg-10 justify-content-center">
						<div className="admin-dashboard-number font-weight-bold pr-2  mb-0 pb-0"> 100 </div> 
						<div className="pt-md-5 m-0 pr-2 ">	   
				   			 <h2 className=" font-weight-bold mb-0"> SUBSCRIBERS </h2 > 
				   			 <h5 className=""> Lorem ipsum dolor sit amet, consectetur adipisicing elit</h5>
						</div> 
						<div className="pt-0 pt-sm-5 col-8 col-lg-2 col-sm-3 order-sm-2 ">
				   			<div className="add-button text-center p-2 ml-2 float-left container-fluid">View All</div>
						</div>
					</div>
				</div>

				<div className="d-flex ml-0 mr-0 col-12 justify-content-center align-items-center pb-2 pb-sm-0">
					<div className="light-beige d-sm-flex mb-2 col-12 col-lg-10 justify-content-center">
						<div className="admin-dashboard-number font-weight-bold pr-2  mb-0 pb-0"> 100 </div> 
						<div className="pt-md-5 m-0 pr-2 ">	   
				   			 <h2 className=" font-weight-bold mb-0"> PRODUCTS SOLD </h2 > 
				   			 <h5 className=""> Lorem ipsum dolor sit amet, consectetur adipisicing elit</h5>
						</div> 
						<div className="pt-0 pt-sm-5 col-8 col-lg-2 col-sm-3 order-sm-2 ">
				   			<div className="add-button text-center p-2 ml-2 float-left container-fluid">View All</div>
						</div>
					</div>
				</div>

				<div className="d-flex ml-0 mr-0 col-12 justify-content-center align-items-center pb-2 pb-sm-0">
					<div className="light-beige d-sm-flex mb-2 col-12 col-lg-10 justify-content-center">
						<div className="admin-dashboard-number font-weight-bold pr-2  mb-0 pb-0"> 100 </div> 
						<div className="pt-md-5 m-0 pr-2 ">	   
				   			 <h2 className=" font-weight-bold mb-0"> ORDERS </h2 > 
				   			 <h5 className=""> Lorem ipsum dolor sit amet, consectetur adipisicing elit</h5>
						</div> 
						<div className="pt-0 pt-sm-5 col-8 col-lg-2 col-sm-3 order-sm-2 ">
				   			<div className="add-button text-center p-2 ml-2 float-left container-fluid">View All</div>
						</div>
					</div>
				</div>
				</div>
			 </Row>

		)
}