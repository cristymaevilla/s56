import {useState} from 'react';

import PropTypes from 'prop-types'
import { Button, Card, Row, Col }from 'react-bootstrap';
import Image from 'react-bootstrap/Image'
import productImage from '../images/product.png'; 

export default function ProductCard({productProp}){

	const{_id, image, name, price}=productProp;
	
	return( 
	
		
				
			<div className=" col-12 col-sm-6 col-md-4 col-lg-3 container-fluid pl-3 pr-3">	
				<div className="m-0 p-0 border-0 text-center text-sm-left dark-beige ">
				  <Card.Body>
					<div className="container-fluid">
						<Image src={productImage} fluid />
					</div>
				    <Card.Subtitle>{name}</Card.Subtitle>
				    <h5>Php {price}</h5>
				    <div className="d-flex justify-content-center container-fluid p-0 mr-md-0">
				
				    		<span className="item1  pl-3 pr-3">1</span>
				    		<span className="item2 pl-2 pr-2">+</span>
				    
				    	<span className="add-button text-center col-6 align-middle pt-1 ml-2">Add</span>
				    </div>
				  </Card.Body>
				</div>
			</div>



		)
}

// checks the validity of proptypes
ProductCard.propTypes = {
	// shape()-if prop object conforms with the specific shape
	product:PropTypes.shape({
		name:PropTypes.string.isRequired,
		name:PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}