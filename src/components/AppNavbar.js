// Import necessary components from react-bootstrap
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Container }from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../user-context';
import '../App.css';
// AppNavbar component
export default function AppNavbar(){
	// const{user} =useContext(UserContext);
	return (
		  <Navbar className="dark-beige fixed-top "expand="lg">
		   
		      <Navbar.Brand as={NavLink} to="/" exact>Majestea</Navbar.Brand>
		      <Navbar.Toggle aria-controls="basic-navbar-nav" />
		      <Navbar.Collapse id="basic-navbar-nav" className="border-0 rounded-0">
		        <Nav className="ml-auto">
		          <Nav.Link as={NavLink} to="/products" exact>Shop</Nav.Link>
		          <Nav.Link as={NavLink} to="/login" exact>LogIn</Nav.Link>   
		        </Nav>
		      </Navbar.Collapse>
		 
		  </Navbar>
		)
}
