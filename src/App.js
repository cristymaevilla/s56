
import {Fragment} from 'react';
import {useState, useEffect} from'react';
import {Container} from 'react-bootstrap';
import AppNavbar from './components/AppNavbar';
import{BrowserRouter as Router} from 'react-router-dom';
import {Route, Switch} from 'react-router-dom';

import {UserProvider} from "./user-context"
import Home from './pages/Home';
import Products from './pages/products';
import SignUp from './pages/signup';
import Login from './pages/login';
import AdminDashboard from './pages/admin-dashboard';
import './App.css';
// enclose all your components inside fragments for them to be usable
function App() {
  const [user, setUser]=useState({
    id:null,
    isAdmin:null
  })

  const unsetUser = ()=>{
    localStorage.clear();
  }
    useEffect(()=>{
      console.log(user);
      console.log(localStorage);
    }, [user])

  return (
            <UserProvider value={{user, setUser, unsetUser}}>
            <Router>
            <div className="dark-beige p-0 m-0">
                <AppNavbar />
                <Container className="pr-0 pl-0 ">
                  <Switch>
                     <Route exact path ="/products" component ={Products} />
                     <Route exact path ="/login" component ={Login} />
                     <Route exact path ="/admin" component ={AdminDashboard} />
                     <Route exact path ="/register" component ={SignUp} />
                  </Switch>
                </Container>
                </div>
            </Router>
            </UserProvider>


  );
}

export default App;


